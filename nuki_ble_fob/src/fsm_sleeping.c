#include "fsm.h"

#include <zephyr/pm/pm.h>
#include <hal/nrf_gpio.h>

LOG_MODULE_REGISTER(sleeping, APP_LOG_LEVEL);

FSM_FUNC(sleeping,entry)
{
    LOG_DBG("sleeping_entry\n");
    
    LOG_INF("Enabling interrupts");
    nrf_gpio_cfg_input(NRF_DT_GPIOS_TO_PSEL(DT_ALIAS(sw0), gpios),
			   NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_sense_set(NRF_DT_GPIOS_TO_PSEL(DT_ALIAS(sw0), gpios),
			       NRF_GPIO_PIN_SENSE_LOW);

};

FSM_FUNC(sleeping,run)
{
    LOG_DBG("sleeping_run\n");
    
    
    LOG_INF("Turn system off\n");
	

	pm_state_force(0u, &(struct pm_state_info){PM_STATE_SOFT_OFF, 0, 0});

	// go take a nap
	k_msleep(1);

	LOG_ERR("ERROR: System off failed\r");

    while(true) 
	{
		k_msleep(1);
	}

};


FSM_FUNC(sleeping,exit)
{
    LOG_DBG("sleeping_exit. \n");
};