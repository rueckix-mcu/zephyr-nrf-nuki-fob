/**
 * @file main.c
 * @author rückix
 * @brief 
 * @version 0.1
 * @date 2022-06-24
 * 
 * @copyright Copyright (c) 2022
 * 
 * 
 * Main loop
 * See samples for a lot of inspiration: https://github.com/zephyrproject-rtos/zephyr/tree/main/samples
 */



#include "log.h"
LOG_MODULE_REGISTER(app, APP_LOG_LEVEL);


#include "fsm.h"

/**
 * @brief TODO LIST
 *  --	1. BUTTON IO and input parsing
 * 	-- 2. Blink Code
 * 	3. BLE scan and filtering
 * 	4. Crypto
 * 	5. Pairing
 * 	6. Authentication
 * 	7. Commanding
 * 	8. Error handling
 *  9. DFU
 * 	10. Cleanup and optimization 
 */


/**
 * @brief TODO: rewrite
 * 
 * - Handle delayed execution
 * 	every button press restarts a timer. No further press => execute action
 * 	https://elixir.bootlin.com/zephyr/v1.14.0-rc3/source/samples/boards/nrf52/mesh/onoff-app/src/main.c
 * - Create blink code
 * -- short (open)
 * -- long (close)
 * -- Extralong (battery)
 * -- always on (pairing mode)
 * 
 * - measure voltage, do battery warning + delayed execution
 * -- https://docs.zephyrproject.org/latest/hardware/peripherals/adc.html
 * 
 * - bluetooth
 * -- BLE stack
 * -- NUKI specifics
 * 
 * - OTA
 * ---- 5x press, is there a standard library?
 * 
 * - refactor
 * 
 * - EEPROM for bluetooth pairing??
 * 
 * 
 *  IDEA
 * separate thread for input button handling
 * emit event when done, main thread waits for it and takes over
 * 
 * port the BLE library to Zeyphyr
 * https://github.com/I-Connect/NukiBleEsp32/tree/main/src
 * https://github.com/espressif/arduino-esp32/blob/master/libraries/Preferences/src/Preferences.cpp
 * 
 */




void main(void)
{

	LOG_INF("\r%s Starting beacon\r", CONFIG_BOARD);
	LOG_DBG("Starting state machine\n");

	run_fsm();

};
