#include "fsm.h"

LOG_MODULE_REGISTER(authenticating, APP_LOG_LEVEL);

FSM_FUNC(authenticating,entry)
{
    LOG_DBG("authenticating_entry\n");
};

FSM_FUNC(authenticating,run)
{
    LOG_DBG("authenticating_run\n");

    // request challenge, respond with MAC


    FSM_NEXT_STATE(COMMANDING);
};


FSM_FUNC(authenticating,exit)
{
    LOG_DBG("authenticating_exit\n");
};