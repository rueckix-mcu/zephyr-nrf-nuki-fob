#pragma once

enum ERROR_CODES
{
    ERR_NONE = 0,
};

enum BLINK_CODES
{
    BLINK_FUNC1 = 0, 
    BLINK_FUNC2 = 1,
    BLINK_PAIR = 2,
    BLINK_DFU = 3,
    BLINK_LOW_BATT = 4
};


static const char* ERR_MESSAGES[] =
{
 [ERR_NONE] = "",
};

// TODO: add array with error messages
// TOOD: add array with blink codes