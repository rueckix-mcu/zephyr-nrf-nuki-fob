#include "gpio.h"

LOG_MODULE_REGISTER(gpio, APP_LOG_LEVEL);

static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET_OR(SW0_NODE, gpios, {0});
static struct gpio_callback button_cb_data;



bool get_button_pressed()
{
	// return true iff button is pulled low

	return (gpio_pin_get_dt(&button) == 0);
}

int get_button_count()
{
	return num_button_press;
}

/**
 * @brief Setup of GPIOs
 * 
 */
void register_gpio(gpio_callback_handler_t button_pressed)
{
    int ret;

	/* Initialize the button debouncer */
	last_button_press_time = k_uptime_get_32();
	num_button_press = 0;

	LOG_INF("Register button interrupt\r");
	if (!device_is_ready(button.port)) {
		LOG_ERR("Error: button device %s is not ready\r",
		       button.port->name);
		return;
	}

	ret = gpio_pin_configure_dt(&button, GPIO_INPUT);
	if (ret != 0) {
		LOG_ERR("Error %d: failed to configure %s pin %d\r",
		       ret, button.port->name, button.pin);
		return;
	}

	ret = gpio_pin_interrupt_configure_dt(&button,
					      GPIO_INT_EDGE_TO_INACTIVE);
	if (ret != 0) {
		LOG_ERR("Error %d: failed to configure interrupt on %s pin %d\r",
			ret, button.port->name, button.pin);
		return;
	}

	gpio_init_callback(&button_cb_data, button_pressed, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);
	LOG_INF("Set up button at %s pin %d\r", button.port->name, button.pin);


	LOG_INF("Setup LED\r");
	if (!device_is_ready(led.port)) {
		LOG_ERR("LED not ready\r");
		return;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		LOG_ERR("LED pin not configurable\r");
		return;
	}
	
	LOG_INF("Set up LED at %s pin %d\r", led.port->name, led.pin);

}

void led_on()
{
	gpio_pin_set_dt(&led, 0);
}

void led_off()
{
	gpio_pin_set_dt(&led, 1);
}