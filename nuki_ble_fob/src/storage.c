#include "storage.h"
#include "log.h"

LOG_MODULE_REGISTER(storage, APP_LOG_LEVEL);

static struct nvs_fs fs;


bool storage_open()
{
    LOG_DBG("storage_open\n");

    int rc = 0;
    struct flash_pages_info info;

	
    fs.flash_device = FLASH_AREA_DEVICE(STORAGE_NODE_LABEL);
	if (!device_is_ready(fs.flash_device)) {
		LOG_ERR("Flash device %s is not ready\n", fs.flash_device->name);
		return false;
	}

	fs.offset = FLASH_AREA_OFFSET(storage);
	rc = flash_get_page_info_by_offs(fs.flash_device, fs.offset, &info);
	if (rc) {
		LOG_ERR("Unable to get page info\n");
		return false;
	}

	fs.sector_size = info.size;
	fs.sector_count = 3U;

	rc = nvs_mount(&fs);
	if (rc) {
		LOG_ERR("Flash Init failed\n");
		return false;
	}

    return true;
}

void storage_reset()
{
    LOG_DBG("storage_reset\n");
    memset(&fs, 0, sizeof(fs));
}


bool storage_read(s_nuki *nuki, uint8_t index)
{
    LOG_DBG("storage_read\n");
    if (!nuki)
    {
        LOG_ERR("Null poitner error\n");
        return false;
    }


    if (!fs.ready)
    {
        LOG_ERR("Flash not ready\n");
        return false;
    }

    if (index > DEVICE_MAX)
    {   
        LOG_ERR("Index out of bounds\n");
        return false;
    }

    int rc = 0;
    rc = nvs_read(&fs, index, nuki, sizeof(nuki));
    if (rc < 0)
    {  
        LOG_ERR("Error reading from flash at index %d\n", index);
        return false;
    }

    return true;

}

bool storage_write(s_nuki *nuki, uint8_t index)
{
    LOG_DBG("storage_write\n");
    if (!nuki)
    {
        LOG_ERR("Null poitner error\n");
        return false;
    }

    if (!fs.ready)
    {
        LOG_ERR("Flash not ready\n");
        return false;
    }

    if (index > DEVICE_MAX)
    {   
        LOG_ERR("Index out of bounds\n");
        return false;
    }
    
    int rc = 0;
    rc = nvs_write(&fs, index, nuki, sizeof(nuki));
    if (rc < 0)
    {  
        LOG_ERR("Error writing to flash at index %d\n", index);
        return false;
    }
    
    return true;

}
