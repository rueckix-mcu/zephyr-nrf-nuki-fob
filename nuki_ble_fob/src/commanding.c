#include "fsm.h"

LOG_MODULE_REGISTER(commanding, APP_LOG_LEVEL);

FSM_FUNC(commanding,entry)
{
    LOG_DBG("commanding_entry\n");
};

FSM_FUNC(commanding,run)
{
    LOG_DBG("commanding_run\n");
    FSM_NEXT_STATE(SLEEPING);


    // generate nuki protocol message and parse response
    // auth token?
};


FSM_FUNC(commanding,exit)
{
    LOG_DBG("commanding_exit\n");
};