#include "fsm.h"

LOG_MODULE_REGISTER(scanning, APP_LOG_LEVEL);


#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>




static void scan_cb(const bt_addr_le_t *addr, int8_t rssi, uint8_t adv_type,
		    struct net_buf_simple *buf)
{
	
	char addr_str[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(addr, addr_str, sizeof(addr_str));
	LOG_INF("Device found: %s (RSSI %d)\n", addr_str, rssi);

	
}




FSM_FUNC(scanning,entry)
{
    LOG_DBG("scanning_entry\n");

    struct bt_le_scan_param scan_param = {
		.type       = BT_HCI_LE_SCAN_PASSIVE,
		.options    = BT_LE_SCAN_OPT_NONE,
		.interval   = 0x0010,
		.window     = 0x0010,
	};
	int err;

	LOG_INF("Starting BLE Scanner\n");

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(NULL);
	if (err) {
		LOG_INF("Bluetooth init failed (err %d)\n", err);
		return;
	}

    LOG_INF("Bluetooth initialized\n");

	err = bt_le_scan_start(&scan_param, scan_cb);
	if (err) {
		LOG_INF("Starting scanning failed (err %d)\n", err);
		return;
	}

};

FSM_FUNC(scanning,run)
{

	// filter for NUKI devices
	// Look for NUKI device with strongest signal
	// start pairing process

    // TODO: Timer
    // TODO: Nuki filtering

    LOG_DBG("scanning_run\n");

    if (FSM_STATE()->pairing_mode)
    {
        LOG_DBG("pairing mode is ON\n");
        FSM_NEXT_STATE(PAIRING);
    }
    else 
    {        
        LOG_DBG("pairing mode is off\n");
        FSM_NEXT_STATE(AUTHENTICATING);
    }


    // scan for devices, take the nuki with best signal


    // look for NUKI service id
    // if we are in pairing mode, look for pairing ready device (take first)



    // TODO: move to authenticating if already paired
};


FSM_FUNC(scanning,exit)
{
    LOG_DBG("scanning_exit\n");
};