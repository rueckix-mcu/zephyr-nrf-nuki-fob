
#pragma once 

#include <zephyr/smf.h>

#include "fsm_state.h"
#include "log.h"
#include "config.h"



// TODO: add necessary bluetooth state structure
// TODO: add config structure for pairing information, supporting 1 lock only (the closest one)


#define FSM_FUNC(X,Y) void X## _ ##Y(void *o)
#define FSM_STATE()   ((s_state*)o)
#define FSM_NEXT_STATE(X) smf_set_state(SMF_CTX(o), &fsm[X]);

#define DECL_FSM_REGULAR_FUNC(X) \
            FSM_FUNC(X, entry); \
            FSM_FUNC(X, run); \
            FSM_FUNC(X, exit);

#define DECL_FSM_PARENT_FUNC(X) \
    FSM_FUNC(X, entry); \
    FSM_FUNC(X, exit);


DECL_FSM_REGULAR_FUNC(booting);

DECL_FSM_PARENT_FUNC(io_parent);


FSM_FUNC(io_0, run);
FSM_FUNC(io_0_10s, run);
FSM_FUNC(io_1, run);
FSM_FUNC(io_2, run);
FSM_FUNC(io_3, run);
FSM_FUNC(io_3_10s, run);

DECL_FSM_REGULAR_FUNC(scanning);
DECL_FSM_REGULAR_FUNC(pairing);
DECL_FSM_REGULAR_FUNC(authenticating);
DECL_FSM_REGULAR_FUNC(commanding);
DECL_FSM_REGULAR_FUNC(failing);
DECL_FSM_REGULAR_FUNC(sleeping);
DECL_FSM_REGULAR_FUNC(dfu);


/* List of demo states */
typedef enum fsm_states { BOOTING, IO_PARENT, IO_0, IO_0_10s, IO_1, IO_2, IO_3, IO_3_10s, DFU, SCANNING, PAIRING, AUTHENTICATING, COMMANDING, FAILING, SLEEPING } fsm_states;



/* Populate state table */
static const struct smf_state fsm[] = {
        
        [BOOTING] = SMF_CREATE_STATE(booting_entry, booting_run, booting_exit, NULL),
		[IO_PARENT] = SMF_CREATE_STATE(io_parent_entry, NULL, io_parent_exit, NULL),

        /* Child states do not have entry or exit actions */
        /* Parent state does not have a run action */
        [IO_0] = SMF_CREATE_STATE(NULL, io_0_run, NULL, &fsm[IO_PARENT]),
        [IO_0_10s] = SMF_CREATE_STATE(NULL, io_0_10s_run, NULL, &fsm[IO_PARENT]),
        [IO_1] = SMF_CREATE_STATE(NULL, io_1_run, NULL, &fsm[IO_PARENT]),
		[IO_2] = SMF_CREATE_STATE(NULL, io_2_run, NULL, &fsm[IO_PARENT]),
		[IO_3] = SMF_CREATE_STATE(NULL, io_3_run, NULL, &fsm[IO_PARENT]),
		[IO_3_10s] = SMF_CREATE_STATE(NULL, io_3_10s_run, NULL, &fsm[IO_PARENT]),

        [DFU] = SMF_CREATE_STATE(dfu_entry, dfu_run, dfu_exit, NULL),
        [SCANNING] = SMF_CREATE_STATE(scanning_entry, scanning_run, scanning_exit, NULL),
        [PAIRING] = SMF_CREATE_STATE(pairing_entry, pairing_run, pairing_exit, NULL),
		[AUTHENTICATING] = SMF_CREATE_STATE(authenticating_entry, authenticating_run, authenticating_exit, NULL),
		[COMMANDING] = SMF_CREATE_STATE(commanding_entry, commanding_run, commanding_exit, NULL),
		[FAILING] = SMF_CREATE_STATE(failing_entry, failing_run, failing_exit, NULL),
		[SLEEPING] = SMF_CREATE_STATE(sleeping_entry, sleeping_run, sleeping_exit, NULL)
};


void run_fsm();
