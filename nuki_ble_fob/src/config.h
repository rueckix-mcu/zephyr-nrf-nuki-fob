#pragma once

/**
 * @brief Set warning threshold for battery in mV
 * 
 */
#ifndef BATTERY_LOW_THRESHOLD
#define BATTERY_LOW_THRESHOLD 2600
#endif


/**
 * @brief Configure default log level=DISABLE
 * 
 */
#ifndef APP_LOG_LEVEL
    #define APP_LOG_LEVEL LOG_LEVEL_NONE
#endif


/**
 * @brief Period to wait until for additional button presses
 * 
 */
#ifndef WAIT_FOR_INPUT_S
#define  WAIT_FOR_INPUT_S 1U
#endif


