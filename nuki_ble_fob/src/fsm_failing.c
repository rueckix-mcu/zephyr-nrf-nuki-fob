#include "fsm.h"
#include "error.h"

LOG_MODULE_REGISTER(failing, APP_LOG_LEVEL);

FSM_FUNC(failing,entry)
{
    LOG_DBG("failing_entry\n");
};

FSM_FUNC(failing,run)
{
    LOG_DBG("failing_run\n");
    // TODO: print error
    // blink code??
    
    FSM_NEXT_STATE(SLEEPING);
};


FSM_FUNC(failing,exit)
{
    LOG_DBG("failing_exit\n");
};