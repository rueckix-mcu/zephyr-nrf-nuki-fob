
#include "nuki.h"
#include "error.h"

/* User defined object */
typedef struct s_state {
        /* This must be first */
        struct smf_ctx ctx;
                
        /* Other state specific data add here */
        struct s_nuki current;
        uint8_t command;
        uint8_t error;
        bool battery_low;
        bool pairing_mode;
} __attribute__((packed, aligned(4))) s_state;
