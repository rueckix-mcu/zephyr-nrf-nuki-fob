#include "fsm.h"
#include "gpio.h"




LOG_MODULE_REGISTER(fsm_io, APP_LOG_LEVEL);

struct k_timer button_timer;
struct k_timer button_hold_timer;
static bool button_expired;
static uint32_t button_press_count;
static bool button_held;


static void button_timer_cb(struct k_timer *work)
{
    LOG_INF("Button timer expired\n");

  	uint32_t num = get_button_count();
    LOG_INF("Button count: %d\r", num);

    button_expired = true;
    button_press_count = num;
    
}

static void blink_code(uint32_t code)
{
    switch(code)
    {
        case BLINK_FUNC1:
            led_on();
            k_sleep(K_MSEC(500));
            led_off();
        break;

        case BLINK_FUNC2:
            led_on();
            k_sleep(K_MSEC(500));
            led_off();
            k_sleep(K_MSEC(500));
            led_on();
            k_sleep(K_MSEC(500));
            led_off();
        break;

        case BLINK_PAIR:
            led_on();
        break;

        case BLINK_DFU:
            for (int i = 0; i < 5; i++)
            {  
                led_on();
                k_sleep(K_MSEC(200));
                led_off();
                k_sleep(K_MSEC(200));
            }
            led_on();
        break;

        case BLINK_LOW_BATT:
            // this delays the next action for a noticeable 10 seconds
            for (int i = 0; i < 25; i++)
            {  
                led_on();
                k_sleep(K_MSEC(200));
                led_off();
                k_sleep(K_MSEC(200));
            }
            
            led_on();
            
        break;

            
    }
}

/**
 * @brief Button callback function
 * 
 * @param dev 
 * @param cb 
 * @param pins 
 */
static void button_pressed(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{

	LOG_INF("button pressed\n");
	uint32_t time = k_uptime_get_32();

	/* debounce the switch */
	if (time < last_button_press_time + BUTTON_DEBOUNCE_DELAY_MS) {
		last_button_press_time = time;
		return;
	}
	
	num_button_press++;
	last_button_press_time = time;

    // restart / restart 1 second timer
    k_timer_start(&button_timer, K_SECONDS(1), K_NO_WAIT);

    
	LOG_INF("Button pressed at %" PRIu32 "\r", k_cycle_get_32());
}



FSM_FUNC(io_parent,entry)
{
    LOG_DBG("io_parent_entry\n");
    
    // setup button handler
    register_gpio(button_pressed);	
    
    button_expired = false;
    button_press_count = 0;
    button_held = true;

    // setup wait timer between button presses
    k_timer_init(&button_timer, button_timer_cb, NULL);

    // start 10+1 seconds wait timer to check if the button is held down
    k_timer_init(&button_hold_timer, NULL, NULL);
    k_timer_start(&button_hold_timer, K_SECONDS(10+1), K_NO_WAIT);
};


FSM_FUNC(io_parent,exit)
{
    LOG_DBG("io_parent_exit\n");
    
    // stop timers
    k_timer_stop(&button_timer);
    k_timer_stop(&button_hold_timer);

    // TODO  deregister button interrupt

    if (FSM_STATE()->battery_low)
        blink_code(BLINK_LOW_BATT);
};


FSM_FUNC(io_0,run)
{
    LOG_DBG("io_0_run\n");


    // check if button was relased before timer expiration
    if (!get_button_pressed() && k_timer_remaining_get(&button_hold_timer) > 0)
    {
        button_held = false;
    }


    // if the button has been held until timer expiration, transition to next state
    if (button_held && k_timer_remaining_get(&button_hold_timer) == 0)
    {
        FSM_NEXT_STATE(IO_0_10s);
        return;
    }

    // if the button has been released at least once, we can process the next state transitions
    if (!button_held && button_expired)
    {
        switch (button_press_count)
        {
            case 1: 
                FSM_NEXT_STATE(IO_1);
                break;
            case 2: 
                FSM_NEXT_STATE(IO_2);
                break;
            case 3: 
                FSM_NEXT_STATE(IO_3);
                // start another hold timer to allow for 2 presses + 10 seconds hold
                button_held = true;
                k_timer_start(&button_hold_timer, K_SECONDS(10), K_NO_WAIT);
                break;
        }
    }

};


FSM_FUNC(io_0_10s,run)
{
    LOG_DBG("io_0_10s_run\n");

    FSM_STATE()->pairing_mode = true;
    FSM_NEXT_STATE(SCANNING);
    
    blink_code(BLINK_PAIR);
};


FSM_FUNC(io_1,run)
{
    LOG_DBG("io_1_run\n");
    FSM_STATE()->command = CMD_OPEN;

    // start wait timer
    // upon expiration, transition to SCAN

    FSM_NEXT_STATE(SCANNING);

    blink_code(BLINK_FUNC1);
};


FSM_FUNC(io_2,run)
{
    LOG_DBG("io_2_run\n");
    FSM_STATE()->command = CMD_CLOSE;

    // start wait timer
    // upon expiration, transition to SCAN
    FSM_NEXT_STATE(SCANNING);

    blink_code(BLINK_FUNC2);
};


FSM_FUNC(io_3,run)
{
    LOG_DBG("io_3_run\n");

    // check if button was relased before timer expiration
    if (!get_button_pressed() && k_timer_remaining_get(&button_hold_timer) > 0)
    {
        button_held = false;
        FSM_NEXT_STATE(SLEEPING);
        return;
    }

    // if the button has been held until timer expiration, transition to next state
    if (button_held && k_timer_remaining_get(&button_hold_timer) == 0)
    {
        FSM_NEXT_STATE(IO_3_10s);
        return;
    }
    
};


FSM_FUNC(io_3_10s,run)
{
    LOG_DBG("io_3_10s_run\n");
    FSM_NEXT_STATE(DFU);
    blink_code(BLINK_DFU);
};
