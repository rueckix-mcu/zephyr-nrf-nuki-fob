#include <zephyr/drivers/gpio.h>


#include "log.h"




/**
 * @brief Get LED from device tree
 * 
 */
#define LED0_NODE DT_ALIAS(led0)
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);


#define BUTTON_DEBOUNCE_DELAY_MS 100

static uint32_t last_button_press_time;
static uint32_t num_button_press;

/**
 * @brief Get button from device tree
 * 
 */

#define SW0_NODE	DT_ALIAS(sw0)
#if !DT_NODE_HAS_STATUS(SW0_NODE, okay)
#error "Unsupported board: sw0 devicetree alias is not defined"
#endif


/**
 * @brief Get the button pressed state
 * 
 * @return true  - button pressed
 * @return false  - button released
 */
bool get_button_pressed();

int get_button_count();

/**
 * @brief Setup of GPIOs
 * 
 */
void register_gpio();

void led_on();

void led_off();
