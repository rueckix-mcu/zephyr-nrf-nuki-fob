#include "fsm.h"

LOG_MODULE_REGISTER(booting, APP_LOG_LEVEL);


#include "battery.h"

// TODO: Check if needed

/*

#include <zephyr/init.h>
#include <zephyr/pm/device.h>
#include <zephyr/pm/policy.h>

*/

/* Prevent deep sleep (system off) from being entered on long timeouts
 * or `K_FOREVER` due to the default residency policy.
 *
 * This has to be done before anything tries to sleep, which means
 * before the threading system starts up between PRE_KERNEL_2 and
 * POST_KERNEL.  Do it at the start of PRE_KERNEL_2.
 */
/*
static int disable_ds_1(const struct device *dev)
{
	ARG_UNUSED(dev);

	pm_policy_state_lock_get(PM_STATE_SOFT_OFF); // PM_ALL_SUBSTATES);
	return 0;


}

// prevent sleep during boot
SYS_INIT(disable_ds_1, PRE_KERNEL_2, 0);
*/


/** A discharge curve specific to the power source. 
 * From https://github.com/zephyrproject-rtos/zephyr/tree/main/samples/boards/nrf/battery
*/
static const struct battery_level_point levels[] = {
#if DT_NODE_HAS_PROP(DT_INST(0, voltage_divider), io_channels)
	/* "Curve" here eyeballed from captured data for the [Adafruit
	 * 3.7v 2000 mAh](https://www.adafruit.com/product/2011) LIPO
	 * under full load that started with a charge of 3.96 V and
	 * dropped about linearly to 3.58 V over 15 hours.  It then
	 * dropped rapidly to 3.10 V over one hour, at which point it
	 * stopped transmitting.
	 *
	 * Based on eyeball comparisons we'll say that 15/16 of life
	 * goes between 3.95 and 3.55 V, and 1/16 goes between 3.55 V
	 * and 3.1 V.
	 */

	{ 10000, 3950 },
	{ 625, 3550 },
	{ 0, 3100 },
#else
	/* Linear from maximum voltage to minimum voltage. */
	{ 10000, 3600 },
	{ 0, 1700 },
#endif
};




FSM_FUNC(booting,entry)
{
    

    LOG_DBG("booting_entry\n");

    int rc = battery_measure_enable(true);

	if (rc != 0) {
		LOG_INF("Failed initialize battery measurement: %d\n", rc);
		return;
	}

	int batt_mV = battery_sample();

	if (batt_mV < 0) {
		LOG_INF("Failed to read battery voltage: %d\n",
				batt_mV);
	}
	else
	{
		unsigned int batt_pptt = battery_level_pptt(batt_mV, levels);

		LOG_INF("%d mV; %u pptt\n",
				batt_mV, batt_pptt);

	}
	LOG_INF("Disable: %d\n", battery_measure_enable(false));

    if (batt_mV < BATTERY_LOW_THRESHOLD)
    {
        LOG_WRN("Low Battery Detected\n");
        FSM_STATE()->battery_low = true;
    }


    
};

FSM_FUNC(booting,run)
{
    LOG_DBG("booting_run\n");
    FSM_NEXT_STATE(IO_0);
};


FSM_FUNC(booting,exit)
{
    LOG_DBG("booting_exit\n");
};