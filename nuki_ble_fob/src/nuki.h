/**
 * @file nuki.h
 * @author your name (you@domain.com)
 * @brief Nuki protocol specifics
 * @version 0.1
 * @date 2022-07-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#include <zephyr/sys/crc.h>
#include <tinycrypt/hmac.h>


// TODO
enum nuki_command
{
    CMD_NONE=0,
    CMD_OPEN=1,
    CMD_CLOSE=2,
};



/* Nuki Pairing Context */
typedef struct s_nuki {       
    uint8_t key[32];
    uint32_t auth_id;
    uint32_t app_id;
    uint8_t uuid[6];
} s_nuki;
