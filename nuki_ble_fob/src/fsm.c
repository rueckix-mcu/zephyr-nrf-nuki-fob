#include "fsm.h"

LOG_MODULE_REGISTER(fsm, APP_LOG_LEVEL);
s_state s_obj;

static void init_state()
{
        memset(&s_obj.current, 0, sizeof(s_obj.current));
        s_obj.command = CMD_NONE;
        s_obj.error = ERR_NONE; 
        s_obj.battery_low = false;
        s_obj.pairing_mode = false;

};

void run_fsm() 
{

        LOG_DBG("run_fsm\n");
        int32_t ret;

        /* Set initial state */
        smf_set_initial(SMF_CTX(&s_obj), &fsm[BOOTING]);

        // initialize state
        init_state();
        



        /* Run the state machine */
        while(true) {
                /* State machine terminates if a non-zero value is returned */
                ret = smf_run_state(SMF_CTX(&s_obj));
                if (ret) {
                        /* handle return code and terminate state machine */
                        break;
                }
                
                // TODO: remove delay
                k_msleep(5000);
        }
};
 
