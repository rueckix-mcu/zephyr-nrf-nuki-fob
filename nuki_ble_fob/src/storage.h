#include <zephyr/drivers/flash.h>
#include <zephyr/storage/flash_map.h>
#include <zephyr/fs/nvs.h>
#include <zephyr/device.h>

#include "nuki.h"

// select storage from device tree
#define STORAGE_NODE_LABEL storage


#define DEVICE0 1
#define DEVICE1 DEVICE0+1
#define DEVICE2 DEVICE1+1
#define DEVICE3 DEVICE2+1
#define DEVICE4 DEVICE3+1
#define DEVICE_MAX DEVICE4

bool storage_open();

void storage_reset();

bool storage_read(struct s_nuki *nuki, uint8_t index);

bool storage_write(struct s_nuki *nuki, uint8_t index);

// TODO: switch to settings.h https://docs.zephyrproject.org/3.0.0/reference/settings/index.html?highlight=settings