# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.20.0)

# debug log level
add_definitions(-DAPP_LOG_LEVEL=LOG_LEVEL_DBG)

set(DTC_OVERLAY_FILE board.overlay)

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(nuki_ble_fob)

FILE(GLOB app_sources src/*.c)
target_sources(app PRIVATE ${app_sources})

